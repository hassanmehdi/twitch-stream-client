import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import GoogleAuth from './GoogleAuth'

export class Header extends Component {
  render() {
    return (
      <div className="ui pointing menu">
          <Link to="/" className="item">Streamer</Link>
          <div className="right menu">
            <Link to="/" className="item">All Streamers</Link>
            <Link to="/streams/show" className="item">Show Streams</Link>
            <Link to="/streams/create" className="item">Create Streams</Link>
            <Link to="/streams/edit" className="item">Edit Streams</Link>
            <Link to="/streams/delete" className="item">Delete Streams</Link>
            <div className="item">
              <GoogleAuth/>
            </div>
          </div>
      </div>
    );
  }
}

export default Header;
