import React, { Component } from 'react';
import { connect } from 'react-redux'
import { signIn, signOut } from '../actions'

class GoogleAuth extends Component {
  componentDidMount () {
    const { gapi } = window
    gapi.load('client:auth2', () => {
      gapi.client.init({ 
        client_id: '670657447437-ir8rblmlckcqae78gfjle68rm2cmt013.apps.googleusercontent.com',
        scope: 'email'
      }).then(() => {
        this.auth = window.gapi.auth2.getAuthInstance()
        this.onAuthChange(this.auth.isSignedIn.get())
        this.auth.isSignedIn.listen(this.onAuthChange)
      })
    })
  }

  onAuthChange = (isSignedIn) => {
    isSignedIn ? this.props.signIn(isSignedIn) : this.props.signOut(isSignedIn)
  }

  handleSignIn = () => {
    this.props.signIn(this.auth.signIn())
  }

  handleSignOut = () => {
    this.props.signOut(this.auth.signOut())
  }

  renderLoginButton () {
    if (this.props.isSignedInGoogle === null) {
      return null
    } else if (this.props.isSignedInGoogle) {
      return (
        <button className="ui red google button" onClick={this.handleSignOut}>
          <i className="google icon" />
          Sign Out
        </button>
      )
    }
    
    return (
        <button className="ui blue google button" onClick={this.handleSignIn}>
          <i className="google icon" />
          Sign In with Google
        </button>
      )
  }

  render() {
    console.log(this.props)
    return (
      <div>{ this.renderLoginButton() }</div>
    );
  }
}

const mapStatetoProps = (state) => {
  return {
    isSignedInGoogle: state.auth.isSignedInGoogle
  }
}

export default connect(mapStatetoProps, {signIn, signOut})(GoogleAuth);
