export const signIn = (bool) => {
  return {
    type: 'SIGN_IN',
    payload: bool
  }
}

export const signOut = (bool) => {
  return {
    type: 'SIGN_OUT',
    payload: bool
  }
}
