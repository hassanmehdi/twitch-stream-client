const INITIAL_STATE = {
  isSignedInGoogle: null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'SIGN_IN':
      return {...state, isSignedInGoogle: true}  
    case 'SIGN_OUT':
        return {...state, isSignedInGoogle: false}  
    default:
      return state
  }
}
